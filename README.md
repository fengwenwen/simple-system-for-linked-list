# simple system for linked list  简单的链表系统

#### 项目介绍
使用链表建立一个储存系统。

#### 特点



1. 可以自动剔除重复编号
1. 支持字母和数字混合排序（太长请改define size 默认为5）
1. 自动排序
1. 添加链表后，会可视化显示链表结构

#### 软件架构
软件架构说明
1.模块化
    详情基本都有注释

#### 使用说明

进入后会出现提示文字


- 1 to insert an element into the list.
- 2 to delete an element from the list.
- 3 to end.
- 之后会看见"?"，意思是提示需要进行输入

如果内存空间不足够，会出现not inserted ，否则则可以进行输入

输入模式
1. "?"后输入1，会进入添加模式，并看见提示文字"enter a character or number" 
1. 输入字符后，如果储存成功，会出现可视化链表结构，例如输入a，成功会出现list is \n a-->NULL，并返回菜单
1. 这里是列表文本输入字符如果已经存在，则会出现have same word,然后会出现可视化链表结构，并返回菜单


删除模式
1. "?"后输入2，会进入删除模式，并看见提示文字"enter character or number to be deleted "
1. 输入字符后，如果删除成功，会出现 %s deleted.和可视化链表结构
1. 输入字符如果不存在，则会出现cannot be found the data %s然后会出现可视化链表结构，并返回菜单
1. 如果输入字符后，发现表为空，输出the line is empty

结束程序
1. "?"后输入3,会结束程序







